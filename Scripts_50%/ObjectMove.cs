﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour
{
    public float speed;
    public GameObject _thisgameObject;   
    public GameObject _target;
    public GameObject _BeMove;
    public GameObject _BeMovetarget;
    bool _Open = false;
    // Start is called before the first frame update
    void Start()
    {
        _thisgameObject = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {

        if (_Open == true)
        {
            if (_thisgameObject.transform.position != _target.transform.position)
            {
                float step = speed * Time.deltaTime;
                _thisgameObject.transform.position = Vector3.Lerp(_thisgameObject.transform.position, _target.transform.position, step);
                _BeMove.transform.position = Vector3.Lerp(_BeMove.transform.position, _BeMovetarget.transform.position, step);
            }
        }
    }

    void HitByRaycast()
    {
        if (_thisgameObject.tag == "NeedMove")
        {
            if (Input.GetKey(KeyCode.Mouse0))
                _Open = true;            
        }
    }

}
